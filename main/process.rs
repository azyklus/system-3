/// Creates the initialisation process and calls the primary shell.
pub fn initialise() -> usize {
   0
}

// IMPORTS //

// MODULES //

/// Fair system scheduler implementation.
pub mod schedule;

/// Asynchronous task control block.
pub mod task;

