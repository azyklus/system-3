# sys3

To build this project, you'll need several things:
- The most recent version of LLVM/Clang, which you may obtain here: https://llvm.org/docs/GettingStarted.html
- CMake to build LLVM, which you may obtain here: https://cmake.org/
- The `cargo-make` tool, which you may obtain here: https://github.com/sagiegurari/cargo-make

---

***NOTE: the above details serve only for posterity as of 2023.11***

Trident is an operating system originally developed as part of a series on [Medium](https://medium.com/@zaiqi) and [MBP2](https://mbp2.blog/@az)
before being co-opted into a personal research project.

As of 2023.11, you will need:
- The Rust nightly as of 2023.11.12, which you may obtain through the official website: https://rust-lang.org/learn/get-started
- Docker to run the build environment: https://www.docker.com/get-started/

# Trident

VERSION: 3.0.1/EARLY/UNRELEASED  
LICENSE: [Apache-2.0](https://github.com/azyklus/sys3/blob/trunk/LICENSE)  
README: [Where would you rather be?](https://xkcd.com/650/)  
INFO:  

The upstream kernel, bootloader, and lib(s) source tree can be found here.  

### Usage

TODO: Update this section when userland modules are created.

### Install

##### For Contributors

To build and install TIES, you will need to install several Rust components:
- `rustup install x86_64-unknown-none`
- `rustup component add llvm-tools-preview rust-src`
- `cargo install just`
- A virtual machine, preferably QEMU, though Virtual Box will work fine if you follow the guide to creating a bootable image.

These prerequisites will need to be installed prior to `cargo run`, else you
are liable to experience extreme frustration, anger, and possibly death from the mental
health spiral you will face due in large part to poor documentation by me. You have
been warned, and your trial begins now.

Once the prerequisites are fulfilled, *and if you are using QEMU,* you may simply run `cargo run` and watch the magic
as it unfolds. If you are using VirtualBox or another VM software, or if you are installing to hardware, you will need
to first create a bootable disk image, which you can find in the 
[/docs/contributor](./docs/contributor/creating-bootable-image.md) folder. 

##### For Users

When using this system, it is recommended to install to a virtual machine,
as the kernel is not ready for any level of normal usage.

To install the kernel to your system, you'll need to build it from source
and launch it from QEMU or another virtual machine:

```
git clone https://codeberg.org/mbp2/system-3.git
cd system-3

cargo build # TODO: Update with proper build process
```

### Development

You will eventually be able to develop your own extensions to Trident 3 through a planned Extensions API.

### Contributing

If you'd like to contribute to this project, please [fork](https://github.com/azyklus/sys3/fork) it and
submit pull requests with your desired features.

1. [Fork it.](https://github.com/azyklus/sys3/fork)  
2. ????? (I forgot what went here)  
3. Submit pull request with your feature. ("[FEATURE] describe your feature").  
4. Profit?  

### Useful Links

- [MBP2 Page](https://mbp2.blog/src/@trident)
- [Matrix Room](https://matrix.to/#/%23two-worlds:mozilla.org)
- [Discord Server](https://discord.gg/B9agTdVH4U)
