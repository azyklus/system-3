# Documentation

This folder contains both user-facing and contributor-facing documentation. Please refer to the index below to find what
you seek, and if you seek to add to the project documentation, you must reach out to Kell on any one of the listed platforms.

### Contributor Index
- [Creating a Bootable Disk Image](./contributor/creating-bootable-image.md)